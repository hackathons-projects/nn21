type OpenCvResponseDto = {
  angleMagQuantile050: number
  angleMax: number
  angleMean: number
  angleQuantile010: number
  angleQuantile025: number
  angleQuantile070: number
  frame: number
  id: number
  magnitudeMax: number
  magnitudeMean: number
  magnitudeQuantile010: number
  magnitudeQuantile025: number
  magnitudeQuantile050: number
  magnitudeQuantile070: number
}[]

type OpenCvModel = {
  magnitudeMean: number
}[]
