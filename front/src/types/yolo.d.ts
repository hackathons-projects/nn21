type YoloResponseDto = {
  avr: number
  b1: number
  b2: number
  b3: number
  b4: number
  b5: number
  cameraId: string
  count: number
  frame: number
  id: number
  max: number
  min: number
  n1: number
  n2: number
  n3: number
  n4: number
  n5: number
  sum: number
}[]
