type SpeedModelResponseDto = {
  avr: number
  count: number
  frame: number
  id: number
  max: number
  min: number
  sum: number
}[]
