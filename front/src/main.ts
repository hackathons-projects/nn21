import './style/reset.css'
import './style/base.css'
import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'

axios.defaults.headers = { 'Content-Type': 'application/json' }
axios.defaults.baseURL = 'http://178.154.201.91:8080/'

createApp(App).mount('#app')
