# NN21

Digital rover team

### YOLOv4
в папке darknet

#### маска скорости
nn_speed_detection.ipynb

### Автоэенодкр
nn_autoencoder_experiments.ipynb

### Backend 

http://178.154.201.91:8080/swagger-ui.html 

### FrontEnd 

https://nn-lime.vercel.app/ 

### Figma

https://www.figma.com/file/v4WzgLfDqbOpGWjqMb2T3a/%D0%BF%D1%80%D0%B5%D0%B7%D0%B0?node-id=0%3A1 
