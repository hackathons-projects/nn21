import os
import random
import cv2

PATH_TO_SUN_IMAGE = '/home/user/datasets/Images'

sun_list = None

def load_sun_list():
    result = []
    for dir, dirs, files in os.walk(PATH_TO_SUN_IMAGE):
        for file in files:
            result.append(os.path.join(dir, file))
    return result

def put_sun_image(image, sun_image, x1, y1, x2, y2):
    sun_height, sun_width, sun_channels = sun_image.shape
    sun_x1 = random.randint(0, sun_width - 1)
    sun_x2 = random.randint(sun_x1 + 1, sun_width)
    sun_y1 = random.randint(0, sun_height - 1)
    sun_y2 = random.randint(sun_y1 + 1, sun_height)
    sun_crop = sun_image[sun_y1:sun_y2, sun_x1:sun_x2]
    sun_crop = cv2.resize(sun_crop, (x2 - x1, y2 - y1))
    image[y1:y2, x1:x2] = sun_crop

def sunitize(image, bboxes):
    global sun_list

    result = image.copy()
    for bbox in bboxes:
        if bbox.label == 0:
            while True:
                sun_image_path = random.choice(sun_list)
                print(sun_image_path)
                sun_image = cv2.imread(sun_image_path)
                if sun_image is not None:
                    break
            put_sun_image(result, sun_image, bbox.x1, bbox.y1, bbox.x2, bbox.y2)
    return result

sun_list = load_sun_list()