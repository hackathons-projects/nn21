import re
import darknet
import cv2

def prepare_darknet_config(config_from_path, config_to_path, width, height):
    with open(config_from_path, 'r') as config_from:
        config_data = config_from.read()
    if (width % 32 != 0):
        width = width // 32 * 32 + 32
    if (height % 32 != 0):
        height = height // 32 * 32 + 32
    # width = min(1024, width)
    # height = min(1024, height)
    config_data = re.sub(r'width=.*', 'width=' + str(width), config_data)
    config_data = re.sub(r'height=.*', 'height=' + str(height), config_data)
    with open(config_to_path, 'w') as config_to:
        config_to.write(config_data)

def image_detection(image, network, class_names, thresh):
    # Darknet doesn't accept numpy images.
    # Create one with image we reuse for each detect
    width = darknet.network_width(network)
    height = darknet.network_height(network)
    darknet_image = darknet.make_image(width, height, 3)

    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image_resized = cv2.resize(image_rgb, (width, height),
                               interpolation=cv2.INTER_LINEAR)

    darknet.copy_image_from_bytes(darknet_image, image_resized.tobytes())
    detections = darknet.detect_image(network, class_names, darknet_image, thresh=thresh)
    image_height, image_width, _ = image.shape
    for i, d in enumerate(detections):
        x, y, w, h = d[2]
        x = x / width * image_width
        y = y / height * image_height
        w = w / width * image_width
        h = h / height * image_height
        detections[i] = (d[0], d[1], (x, y, w, h))
    darknet.free_image(darknet_image)
    return detections