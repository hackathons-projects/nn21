import glob
import os

current_dir = os.path.abspath('dataset') # PATH TO IMAGE DIRECTORY

# Percentage of images to be used for the valid set
percentage_test = 10

# Create train.txt and valid.txt
file_train = open('train.txt', 'w')
file_test = open('test.txt', 'w')

# Populate train.txt and valid.txt
counter = 1
index_test = round(100 / percentage_test)
for dir, dirs, files in os.walk(current_dir):
    for file in files:
        if os.path.splitext(file)[-1] == '.jpg':
            if counter == index_test:
                counter = 1
                file_test.write(os.path.join(dir, file) + '\n')
            else:
                file_train.write(os.path.join(dir, file) + '\n')
                counter = counter + 1
