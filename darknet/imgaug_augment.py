# Preparation:
# > sudo pip3 install imgaug
# > sudo pip3 install opencv-python
# > sudo pip3 install imagecorruptions

import os
import cv2
import json
import sequential
import random
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
import xml.etree.ElementTree as ET
from sun_tools import sunitize

PATH_TO_DATASET = 'dataset'
PATH_TO_AUGMENTED_DATASET = 'augmented'
ITERATIONS = 2

CLASS_IDS = dict()
CLASS_IDS['plus'] = 0
CLASS_IDS['car'] = 1
CLASS_IDS['cat'] = 1

dataset_id = None
image_id = 0

def load_dataset_id():
    global dataset_id

    try:
        dataset_id = json.loads(open('config.json', 'r').read())['dataset_id']
    except:
        dataset_id = 0

def save_dataset_id():
    global dataset_id
    open('config.json', 'w').write(json.dumps({ 'dataset_id': dataset_id }))

def decode_class_id(class_name):
    return CLASS_IDS[class_name]

def augment(image_path, info_path, info_ext, need_sunitize = False, sunitize_only = False, debug = False):
    global dataset_id, image_id
    image = cv2.imread(image_path)
    height, width, _ = image.shape
    bboxes_list = []
    if info_ext == '.txt':
        with open(info_path, 'r') as info:
            for line in [x.strip() for x in info.readlines()]:
                print(line)
                if len(line) > 0:
                    line_split = line.split(' ')
                    class_id = int(line_split[0])
                    x, y, w, h = [float(x) for x in line_split[1:]]
                    print((class_id, x, y, w, h))
                    x1 = int(width * (x - w / 2.0))
                    y1 = int(height * (y - h / 2.0))
                    x2 = int(width * (x + w / 2.0))
                    y2 = int(height * (y + h / 2.0))
                    if debug:
                        cv2.rectangle(image, (x1, y1), (x2, y2), (255, 0, 0))
                    bboxes_list.append(BoundingBox(x1=x1, y1=y1, x2=x2, y2=y2, label=class_id))
    else:
        xml_data = ET.parse(info_path)
        print(xml_data)
        for object in xml_data.getroot().findall('object'):
            print(object)
            xml_bndbox = object.find('bndbox')
            if xml_bndbox is None:
                continue
            x1, y1, x2, y2 = int(xml_bndbox.find('xmin').text), int(xml_bndbox.find('ymin').text),\
                             int(xml_bndbox.find('xmax').text), int(xml_bndbox.find('ymax').text)
            if debug:
                cv2.rectangle(image, (x1, y1), (x2, y2), (255, 0, 0))
            bboxes_list.append(BoundingBox(x1=x1, y1=y1, x2=x2, y2=y2, label=decode_class_id(object.find('name').text)))

    if debug:
        cv2.imshow('image', image)
        cv2.waitKey(-1)
    bboxes = BoundingBoxesOnImage(bboxes_list, shape=image.shape)
    save_path = os.path.join(PATH_TO_AUGMENTED_DATASET, str(dataset_id))
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    if need_sunitize:
        if len(bboxes) == 0:
            return
        image_aug = sunitize(image, bboxes)
        if sunitize_only:
            cv2.imwrite(os.path.join(save_path, str(image_id) + '.jpg'), image_aug)
            info_aug = open(os.path.join(save_path, str(image_id) + '.txt'), 'w')

    if need_sunitize and (not sunitize_only):
        image = image_aug
        bboxes = BoundingBoxesOnImage([], shape=image.shape)

    if not sunitize_only:
        image_aug, bboxes_aug = sequential.seq1(image=image, bounding_boxes=bboxes)
        print(bboxes_aug)
        for bbox_aug in bboxes_aug:
            if (bbox_aug.x1 < 0) or (bbox_aug.y1 < 0) or (bbox_aug.x2 >= width) or (bbox_aug.y2 >= height):
                return
        cv2.imwrite(os.path.join(save_path, str(image_id) + '.jpg'), image_aug)
        info_aug = open(os.path.join(save_path, str(image_id) + '.txt'), 'w')
        for bbox_aug in bboxes_aug:
            print(bbox_aug)
            if debug:
                cv2.rectangle(image_aug, (bbox_aug.x1, bbox_aug.y1), (bbox_aug.x2, bbox_aug.y2), (255, 0, 0))
            w = (bbox_aug.x2 - bbox_aug.x1) / width
            h = (bbox_aug.y2 - bbox_aug.y1) / height
            x = bbox_aug.x1 / width + w / 2.0
            y = bbox_aug.y1 / height + h / 2.0
            info_aug.write(str(bbox_aug.label) + ' ' + str(x) + ' ' + str(y) + ' ' + str(w) + ' ' + str(h) + '\r\n')
        if debug:
            cv2.imshow('image_aug', image_aug)
            cv2.waitKey(-1)

    image_id += 1


load_dataset_id()

for i in range(ITERATIONS):
    dataset_id += 1
    save_dataset_id()
    image_id = 0

    for dir, dirs, files in os.walk(PATH_TO_DATASET):
        random.shuffle(files)
        for file in files:
            file_split = os.path.splitext(file)
            if file_split[-1] in ['.jpg']:
                image_path = os.path.join(dir, file)
                print(image_path)
                info_ext = None
                for ext in ['.txt', '.xml']:
                    info_path = os.path.join(dir, ''.join(list(file_split[:-1])) + ext)
                    if os.path.exists(info_path):
                        info_ext = ext
                        break
                if info_ext is not None:
                    print(info_path)
                    # random.choice([True, False])
                    need_sunitize = random.randint(0, 10) == 0 # 10%
                    suniztize_only = need_sunitize and random.choice([True, False])
                    augment(image_path, info_path, info_ext, need_sunitize, suniztize_only)