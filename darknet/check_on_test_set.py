import darknet
import os
import cv2
from darknet_tools import *
import numpy

TEST_SET_LIST_PATH = 'test.txt'
POSTFIX = '-tiny-fox'
WIDTH = 512
HEIGHT = 512

prepare_darknet_config(os.path.join('cfg', 'yolov4' + POSTFIX + '.cfg'), 'yolov4' + POSTFIX + '-temp.cfg', WIDTH, HEIGHT)
network, class_names, class_colors = darknet.load_network(
        'yolov4' + POSTFIX + '-temp.cfg',
        os.path.join('data', 'fox.data'),
        os.path.join('backup', 'yolov4' + POSTFIX + '_final.weights'),
        1
    )

total_images = 0
true_images = 0
false_images = 0
total_iou_plus = 0
count_iou_plus = 0
total_iou_car = 0
count_iou_car = 0
with open(TEST_SET_LIST_PATH, 'r') as test_set_list:
    for line in test_set_list.readlines():
        image_path = line.strip()
        if len(image_path) > 0:
            # print(image_path)
            image = cv2.imread(image_path)
            height, width, _ = image.shape
            total_images += 1
            objects = []
            info_path = ''.join(list(os.path.splitext(image_path)[:-1])) + '.txt'
            objects_mask_plus = numpy.zeros((height, width, 1), numpy.uint8)
            objects_mask_car = numpy.zeros((height, width, 1), numpy.uint8)
            with open(info_path, 'r') as info:
                for line in [x.strip() for x in info.readlines()]:
                    # print(line)
                    if len(line) > 0:
                        line_split = line.split(' ')
                        class_id = int(line_split[0])
                        x, y, w, h = [float(x) for x in line_split[1:]]
                        # print((class_id, x, y, w, h))
                        x1 = int(width * (x - w / 2.0))
                        y1 = int(height * (y - h / 2.0))
                        x2 = int(width * (x + w / 2.0))
                        y2 = int(height * (y + h / 2.0))
                        objects.append((class_id, x1, y1, x2, y2))
                        if class_id == 0:
                            cv2.rectangle(objects_mask_plus, (x1, y1), (x2, y2), (255), -1)
                        else:
                            cv2.rectangle(objects_mask_car, (x1, y1), (x2, y2), (255), -1)
            detections = image_detection(image, network, class_names, 0.5)
            print(detections)

            if ((len(objects) == 0) and (len(detections) == 0)) or ((len(objects) > 0) and (len(detections) > 0)):
                true_images += 1
            else:
                false_images += 1
                print(image_path)
                # cv2.imshow('image', image)
                # cv2.waitKey(-1)

            detections_mask_plus = numpy.zeros((height, width, 1), numpy.uint8)
            detections_mask_car = numpy.zeros((height, width, 1), numpy.uint8)
            for d in detections:
                x, y, w, h = d[2]
                x1 = int(x - w / 2.0)
                y1 = int(y - h / 2.0)
                x2 = int(x + w / 2.0)
                y2 = int(y + h / 2.0)
                if d[0] == 'plus':
                    cv2.rectangle(detections_mask_plus, (x1, y1), (x2, y2), (255), -1)
                else:
                    cv2.rectangle(detections_mask_car, (x1, y1), (x2, y2), (255), -1)
            union_mask_plus = cv2.bitwise_or(objects_mask_plus, detections_mask_plus)
            union_mask_car = cv2.bitwise_or(objects_mask_car, detections_mask_car)
            intersection_mask_plus = cv2.bitwise_and(objects_mask_plus, detections_mask_plus)
            intersection_mask_car = cv2.bitwise_and(objects_mask_car, detections_mask_car)
            try:
                iou_plus = cv2.countNonZero(intersection_mask_plus) / cv2.countNonZero(union_mask_plus)
                total_iou_plus += iou_plus
                count_iou_plus += 1
            except:
                iou_plus = 'Not available'
            try:
                iou_car = cv2.countNonZero(intersection_mask_car) / cv2.countNonZero(union_mask_car)
                total_iou_car += iou_car
                count_iou_car += 1
            except:
                iou_car = 'Not available'
            print((iou_plus, iou_car))


print((total_images, true_images, false_images))
print((total_iou_plus / count_iou_plus, total_iou_car / count_iou_car))
