import ffmpeg
import subprocess
import numpy

def start_ffmpeg_process(filename):
    args = (
        ffmpeg
            .input(filename)
            .output('pipe:', format='rawvideo', pix_fmt='bgr24')
            .compile()
    )
    return subprocess.Popen(args, stdout=subprocess.PIPE)

def read_frame(process, width, height):
    frame_size = width * height * 3
    bytes = process.stdout.read(frame_size)
    if len(bytes) != frame_size:
        return None
    else:
        frame = (
            numpy
            .frombuffer(bytes, numpy.uint8)
            .reshape([height, width, 3])
        )
        return frame